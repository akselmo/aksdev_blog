---
layout: post
title:  "Uploaded my themes to KDE Store"
date:  2022-08-22 21:02:31 +0300
---

Hey, just a quick post, I uploaded some themes I've made for Aurorae and Plasma to KDE store.

They're simple themes that follow your colorscheme but are made for dark color schemes.
The accent themes follow your accent color for the outlines.

The reason I made these themes is that I need a strong outline around my windows and other elements: Otherwise all the windows I have just "blend" together no matter how strong shadow I have enabled.

Aurorae Themes:
- [Without accent color](https://store.kde.org/p/1887649)
- [With accent color](https://store.kde.org/p/1887668)

Plasma Themes:
- [Without accent color](https://store.kde.org/p/1887659)
- [With accent color](https://store.kde.org/p/1887679)

Here's screenshots of them in use:

Without accent outlines:
[![Non-accented theme](/assets/images/kde/no_accent.png)](/assets/images/kde/no_accent.png)

With accent outlines:
[![Accented theme](/assets/images/kde/with_accent.png)](/assets/images/kde/with_accent.png)

And finally, link to the repository: [https://codeberg.org/akselmo/aks_themes](https://codeberg.org/akselmo/aks_themes)

I hope you like them!
