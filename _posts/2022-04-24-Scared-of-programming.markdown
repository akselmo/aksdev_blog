---
layout: post
title:  "(I was) Scared of programming"
date:  2022-04-24 02:35:30 +0300
---

I have always dabbled with programming languages, even as a kid. But I never made anything
with them until I was in my 20s. Most extensive things I did before my 20s was simple 
modifications to some files or dumb tiny javascript files.

The thing is, people always told me programming is very difficult (which is true!) and
I am the kind of person that always downplays their abilites. So I wanted to make programs
and games, but I never wanted to do the actual programming because how people treated it.
I always looked for non-programming game engines and such.

**Programming was scary!**

I was scared of breaking my PC, or just general failure. The latter is of course a personal
problem, but I wish people had told me that it's not as scary as I thought. Failure is big
part of programming, that's how most of us learn. 

Well, eventually I had to overcome my fears: Our class at college had nobody who could do 
programming, so I decided it's time to try at least.. And I got hooked.

I eventually realised it's not that scary and that my mind *can* wrap around these mystical
words I am typing to make things happen. C# was my entry to programming, it was the language that 
got me hooked. Python also made me happy.

**However, there's something I was always still afraid to try: C and C++.**

I was told to not use C unless I "know what I'm doing." It's valid advice, but also 
it was more of this "ooo scary spooky language" thing that made me just ignore it and
keep working with higher level languages. 

I wish that instead I would've been told "It's old and weird, but it can be very very
useful language to know and work with." And nowadays, especially with libraries like Raylib,
it's pretty simple to get going and start making a game. Difficult for sure. But never scary.

Making my Raylib FPS in pure C has been one of the most fun programming projects I've had
in years!

Same thing happened with C++. People told me it's an awful language to work with, I should never
bother with it, yadda yadda. Again, difficult language for sure! Challenging even. But not scary.
I finally decided to work making my ESO Addon Manager in C++ and Qt. Sure it's complicated but also
there is a challenge, a puzzle, that my mind really enjoys!

What I am trying to point here is that..

**Stop scaring people away from trying!**

You're free to have opinions, even scathing ones, of languages. I know I have those about Javascript.
But your dislike may be someones like, which they never get to realise because you scared
them away. Let people try. Let people fail!

I found out about my likes way later because I was spooked away. I would've never learned
how much I actually enjoy writing C if I just hadn't taken the plunge. I went in with complete
"oh god is this going to suck" and I am glad to say, it hasn't sucked at all.

The challenges both C and C++ have given me are great. 
I learned that I enjoy lower level programming, despite it's weird own world where the computer
doesn't help you out of sticky situations! (Like memory handling)

I am no more scared! I am excited!
I just wish I had realised this when I was 15. And that's why I'm ranting about this.

*Okay, well maybe assembly still scares me a bit. But maybe one day I'll tackle that too!* :D 

PS. this is a bit incoherent ramble probably because it's late and I just wanted to get this out of my system lol
