---
layout: post
title:  "Image: Quake Logo ANSII"
date:   2021-08-21 01:33:21 +0300
---

# Quake Logo ANSII

[![QuakeANSII](/assets/images/art/QuakeANSII.png)](/assets/images/art/QuakeANSII.png)

Played around with [REXPaint](https://www.gridsagegames.com/rexpaint/). It works fine under Wine. Felt like making Quake logo because of Quakecon and Quake being 25 years old!
