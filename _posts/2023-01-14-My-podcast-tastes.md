---
layout: post
title:  "My podcast tastes"
date: 2023-01-14 23:02:03 +02:00
---

# My podcast tastes

I just wanted to write this down since I've been talking a bit about podcasts
on fediverse.

I listen a lot of podcasts when gaming, it's kind of a way how I relax.
Nothing like good multiplayer FPS where I can turn off my brains, then I can
listen podcasts on the side. :)

# List of podcasts I listen

Here's some of the podcasts I recommend trying! I'm too lazy to link them
but they're one search away. I've also found these all from [https://podcastindex.org/](https://podcastindex.org/).

## 2.5 Admins
I don't really understand anything they talk about most of the time since it's
very sysadmin stuff, but I still find it entertaining and listen every episode.
Maybe sometimes I learn something.

## Algorithms + Data Structures = Programs
This is a weird podcast. It's couple guys that can't stay on topic. I kinda like it.
I don't listen every episode, especially if they start to ramble on about sports,
but there are good bits about it.

## C++ Club
Very no-nonsense podcast about developments in C++ world. I kinda like the format
and listen it whenever there's an episode, just to learn more about the language
I often use with my FOSS contributions and so on.

## Coding Blocks
General programming podcast, very educational. They mostly talk about webdev from
what I've gathered, so even if it doesn't touch my area of interest, it's still
entertaining.

## Contributor
Interviews with various open-source projects. I like this one a lot, since
it's more about developers being enthusiastic about their projects than
some guy who likes their own voice a lot.

## CoRecursive: Coding Stories
This is one of the best programming podcasts I know. A lot of interesting stories
around programming and computing world. Often feels like I'm in an interesting
history class. (Yes, that's a good thing!)

## CppCast
Apparently they started to make new episodes again by other hosts? Anyhow
I listen this from time to time, there are a lot of things to learn from this
podcast.

## FLOSS Weekly
Interviews of people contributing and maintaining FLOSS projects. Also sometimes
just discussions about some things that may affect FLOSS. I try to listen every episode
but sometimes something just doesn't interest me, and that's fine.

## Fragmented: The Software Podcast
This is more about Android/iOS development. I don't do any of that (yet, maybe in future), but
I still find things to learn from this podcast.

## Koodia pinnan alla
Finnish programming podcast that concentrates more on the backend programming that
users do not necessarily see.

## Late Night Linux, Linux After Dark, Linux Downtime
I like all of these. Covering some Linux news with lots of banter and humor.
There's also more indepth discussions (sometimes heated ones) about Linux and FOSS
related things. I listen every episode and enjoy them a lot.

## Linux Lads
Also entertaining Linux podcast, similar vein to Late Night Linux. Bunch of lads
talking about FOSS.

## Programming Throwdown
More programming interviews. I don't listen all of them, just the ones that I'm interested in.
Can be educational.

## Reality 2.0
General technology podcast. Seems to be around how technology affects us and it's very FOSS-sided,
which I appreciate. However they called Mastodon a "protocol" which is WROOONG. :P

## Talk Python To Me
Python is not my favorite language but I do like listening this one, since I use a lot of Python at work.
It's more than just Python which is also nice.

## The Changelog
A fine podcast, been around a long time. A bit too techbro-ish maybe for my tastes, but aside that there are things to learn.

## Two's Complement
Very VERY educational podcast. That being said I don't always understand what is being talked about, lol. Lots of compilers and testing.

# List of podcasts I used to listen but stopped
I also wanted to mention couple podcasts I wish I could still listen to but.. Well, you'll see.
Basically they do not align with my views anymore. They're free to do whatever they want,
obviously. I'm not here to tell them stop. But here's why *I* stopped listening.

## Jupiter Broadcasting podcasts
I used to listen a lot of these. One of the first Linux related podcasts I listened was by them.
Now there's much [cryptocoin scams](https://web3isgoinggreat.com/), I don't know what I can trust in the podcasts anymore.
Also, the episodes are full of annoying sound effects.

Special mention for Coder Radio: I liked it at one point but when I realised it's all about
being a "troll" to get a "reaction" or something, I kind of lost all my trust towards it.
Then of course on top of that we have the obsession over cryptocoin scams.

## Destination Linux
I liked them at first but..
I had an issue in their Matrix channel and I don't feel like listening it anymore:
I shared a link to an itch.io game bundle but it was deemed "too political." I left the Matrix channel quickly after that.
Petty drama I guess, but I just don't feel it anymore.

# Any suggestions?
Feel free to comment and suggest me any podcasts and Youtube/Peertube channels with good programming
and Linux related content! :) I'm always interested for more.

### Also mentioning my own podcast: AksDev Cast
I run a atleast-once-a-month where I talk about things that I find interesting: Linux, FOSS, games, gamedev... A lot of ranting too about the bullshit I see around.
I also update my community on things I am making or made, be it games or other things.

Basically if you want to listen an angry Finnish guy rant with a shitty microphone.. Well, I don't
understand what kind of masochist you are but that's my podcast lol.

It comes out as a stream at first, then uploaded to the podcast RSS feed.

Check it out here if you're interested: [https://cast.akselmo.dev/](https://cast.akselmo.dev/)