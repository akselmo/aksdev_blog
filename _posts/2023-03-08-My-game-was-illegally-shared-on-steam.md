---
layout: post
title: "My game was illegally uploaded on Steam"
date: 2023-03-08 18:54:50 +02:00
---

Quick post for this time.

Seems my game, [Penance](https://akselmo.itch.io/penance) has been
uploaded on Steam by someone who I don't know, completely
without my permission. They're selling my *free* game and of course
I'm not gonna get any money from their sales.

If you stumble on this game outside of this page: [https://akselmo.itch.io/penance](https://akselmo.itch.io/penance) **then it's not uploaded by me!** It's
also very likely those illegal copies are also bundled with malware.

I have done a DMCA claim for the game already.

I'm not going to link the fake link here just in case, but it's easy to find if you're curious.

Just know that **all my games are on itch.io** [https://akselmo.itch.io](https://akselmo.itch.io)
and all the other possible "copies" are fake and not by me. If I make a Steam page for any of my games in future, you will be let know by my Fediverse account and/or this blog.

**This also serves as a PSA for fellow gamedevs**: Check if your game has been uploaded into some places without your permission! **Especially on Steam!**
I didn't expect this to happen since uploading on Steam costs money, but someone uploaded it anyway without any permission.

Stay safe out there.