---
layout: post
title:  "I was told I don't care"
date:  2022-07-14 16:28:13 +0300
---

Today in Gaming On Linux discord group I was told that "I don't care about my playerbase" by one
person and I got so frustrated I decided to write a blogpost about this. 
That's fucking insulting and just false.

*Note: The mods of the discord group apologized to me and have said this was the persons last warning. The group itself is nice and helpful.*

Anyway, here's the whole story:

We were discussing the Unity+MalwareAdCompanyWhichNameIDontRemember merger.

Paraphrasing, I said something along the lines of

> I find it funny that many gamers don't really care about the game engine unless it tells you
straight up what the engine is.

I may have used "fuck" there somewhere for emphasis. I've been told that I sound often "angry"
through text. English is not my native language so I don't know how to write proper prose that
keeps everyone happy.

Now in the return I got a huge wall of text, that basically boiled down to 

>"You insult gamers, you don't care about gamers. You don't care about your userbase."

Let's go through this BS one by one.

# I don't think any gamer should care about the game engine

My "I find it funny" was mostly aimed at people who go "unity bad peepeepoopoo" and then play Unity
games without knowing they're Unity games, since the game doesn't tell them. When it comes to the
context of the discussion we were having, I thought it was obvious. This could be my bad and I
admit it: I am bad with these sort of things. Heck I went therapy because of it. I'm still learning.

Anyhow. Gamers should NOT NEED TO care what engine is being used. Engine is just a tool. Does anyone care how their
chair or table was made? I doubt many do, and those who do are chair and table connosseurs, and we have those
people in gaming world too. If you do care about the engine, more power to you! You can make educated choices
and that's fantastic.

But the majority should not need to care. And many don't. They double click the icon on their desktop and play.

That is totally fine.

But it's funny when someone cares, says engine x poopoo and is hypocritical about it.

# I care about my userbase. A lot.

Telling me that I don't care about my users and putting words in my mouth definitely made me angery
enough to write this blogpost. 

I have been making games around.. 10 years soon? *Holy shit.* 

During these times, I've published around 5 titles on Itch.io and all of them have gotten feedback.
I love receiving feedback and I've listened, trying to make my newer title always better than the old one.

Then on the whim I made Penance. It has over 14k downloads. It got rough feedback at first. I improved the game,
I even made my own maps for it. The download count around that time just exploded.

I did this because I fucking care. I spent many nights sleep-deprived thinking how to make the game better.
Eventually shit got so tough I had to tell myself "Okay, one last update, and no more for this project."

I was asked to make speedrun pages for Penance: I did. I now moderate it and just couple days ago I got plenty of new
runs. I love seeing my games being speedran.

I've tried my best to keep my fanbase happy and people chat in my Discord group a lot nowadays, mostly about programming
and that makes me super happy. I try to keep that place clean because I care about the quality there.

Now I know I am a no-name gamedev, but I DO A LOT of stuff to keep my lovely community happy.
New gamedevs sometimes join and ask me questions and I do my best to help them.

I care about every single one of you in my Discord/Matrix community. Thank you for staying with me on this ride.

### But you do it all for money!!!!111one

HA! I've spent around like 700€ for assets, engine stuff, etc etc. Guess how much I've made in all of these 10 years?

**250€**.

So I'm like... 450€ loss. Lol. But I don't care about money. I care about making fun games.

Anyhow, that is because my games are pay-what-you-want titles. People are allowed to put 0€ if they wish. Sometimes someone
likes the game enough to pay for it and for that I am very grateful. 

Why pay-what-you-want? Because I care enough that everyone who wants gets to play my games. 

And now I am making completely open source, moddable retro-FPS. Why? Because I care enough that someone might
find it easy way to get into gamedev and find it fun to tinker with.

I love entertaining people, gamedev is for me the easiest way to do so.

To end this incoherent ramble: **Don't ever fucking tell me I don't care.**

Anyhow, if someone doesn't like my stuff, that's fine. The shittalk is not.
If you shittalk me, you're never going to be my customer. I will refuse to sell to you.

**We gamedevs have put up with toxic bullshit long enough.**

Insult my games and whatever, but never tell me I don't care.

Cheers and sorry for this blogpost. I hope I don't have to write any more posts like this.
