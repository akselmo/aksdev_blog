---
layout: post
title:  "Back to Linux!"
date:   2021-07-28 00:48:56 +0300
---

I wanted to give another Linux distro a try, so I installed Kubuntu instead. 
And I'm glad I did!

![My Kubuntu](/assets/images/linux_desktop_2.png)

I feel like [Kubuntu](https://kubuntu.org/) suits my needs way better than Linux Mint: I wanted a simple, light desktop with customization but it stays out of my way when gaming. And that's what KDE is doing. As much as I like XFCE, it's just.. Old. And not much of cool stuff is being made for it anymore. KDE is also easier to customize and let's you do way more things than XFCE, in my opinion. I'm probably wrong but hey.

When it comes to gaming, I found a way to update my [Mesa drivers](https://launchpad.net/~kisak/+archive/ubuntu/kisak-mesa), so they my PC runs games better now than on Linux Mint. On top of that, I realised one big problem why my Linux Mint was having problem with games: It didn't disable composition when playing. 

Composition is what gives eyecandy to your desktop: animations, transperancy, all that. It also seems to eat your perf when playing games, I suspect it's because it keeps updating your desktop in middle of game's draw calls, so it gets wonky. Disabling it helps immensely when playing. I suspect Windows does something like this automatically.

Linux would also do it automatically, but KDE has trouble detecting fullscreen apps when they're ran through Proton, so I have to set every game disable it manually. I hope there will be a fix soon, however thanks to KWin (i assume?) letting me add window behaviors, I can disable composition to every game I want that way.

It's amazing to play all the games I used to play with Windows, but on top of that, having an OS which is just plain fun to use and tinker with is big plus. It feels "free" and less "corporate" than Windows, and I love it. And of course, programming is way much easier on Linux, since I can just install all the dependencies easy and quick through the terminal.

Will definitely try gamedeving this weekend if I got time and right mindset lol, maybe even with Unity.

PS. As you may have noticed, I've added tiny banners to my sidebar! Check the links! There's stuff to be found.