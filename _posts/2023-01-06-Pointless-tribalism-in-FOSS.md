---
layout: post
title:  "Pointless tribalism in FOSS"
date: 2023-01-06 03:30:30 +02:00
---

*Rant incoming. This isn't written that well but idgaf. I just need to get this out of my system.*

I really enjoy chatting in FOSS communities. KDE, Linux in general, other desktop environments, so on and so forth.

What I don't like is that we have these very silly "feuds" over some things.
Like we're some weird tribes, fighting over who has the nicest looking flag.

- It doesn't serve any purpose but putting others down.
- It drives away users, new and old, and potential contributors.
- It makes people afraid to report bugs or ask questions.

**It's entirely pointless.**

I'm not talking about critique. Assholes of the world love to pretend
their behavior is "critique" but rest of us know that's not how it works.
**If it uses inflammatory language and insults, it's not critique.**

I for sure ignore any complaints about my games and projects that start with telling me off, either directly or indirectly.
I do not care at that point. Seethe.

Sure this is more an internet/human problem in general, but
there's nothing to gain from this, especially in FOSS world.
We're a small community, trying to survive against big money corporations.
**FOSS projects don't suddenly get more money if people go "X is better and Y users are dumby smelly."**
There just is no reason to be an ass about any of this.

Why jeopardise all these years of survival for some shit-talk?

The usual one is the GNOME vs KDE "debate", that is so pointless I don't even
understand why it exists.

The answer to these "debates" is simple: Try all desktop environments that interest you.
Stick with the one that you like best.

No need to bring flamewars into this. Completely pointless.

It wastes everyone's time.

Desktop environments all have their strengths and weaknesses. It's entirely
subjective. Yes, there are cases where you have to measure security and other such things: You don't have to put down the others if they don't fit your usecase.

It's like fighting over a fucking desktop wallpaper.

When it comes to the Flatpak vs Snap vs Appimages, I have the same answer: Use what works for you.
Avoid those that do not work for you. Critique well and respectfully.

I personally don't see any of the three to be something I prefer. I like my distro provide the packages for me.
But I do not bring people down because they choose to use one of those packaging methods. That would be asinine.

All these fights help burn bridges between projects and worsens the ecosystem for the whole community.

Leave the useless fights to proprietary world. We have learned from that world that you have
to "fight" to have your cake and eat it too.
Luckily in FOSS world, we don't need that. We can all have our cakes, share recipes, improve them, make entirely our own... And eat them as well.

# But I don't like a thing :(

Give **good** critique about why you don't like it. My rule of thumb is:

- Compliment the things you like first
    - Optional but seeing someone go "I use X because I really like it, I just wish Y would.." is super motivating! Like hell yeah let's make things better for you if we can.
- Be respectful!
- Avoid inflammatory language or snarky embellishments: They just slow down the process
- Consider: Maybe the thing is just not made for you? Maybe there's better alternative for your usecase?

If the maintainer(s) of the project do not see your critique an issue, you can always:
1. Fork
2. Improve, either alone or in a team that shares your view
3. Offer to contribute your changes to upstream

Repeat and improve together. Proprietary world doesn't have that strength.

And for those who are going to say "I can't code, improve or w/e" then
I'm sorry but the nature of computers is that you either need programming knowhow or
you need to know people who can do things for you. Ask around. Talk to people. Communicate.
**Without being an ass about it!**

We're all in this together. We should damn well act like it.
