---
layout: post
title:  "Image: Lizard Vector"
date:   2021-08-03 01:38:52 +0300
---

# Lizard Vector

[![LizardVector](/assets/images/art/lizardvector.png)](/assets/images/art/lizardvector.png)

[![LizardVector_dither](/assets/images/art/lizardvector_dither.png)](/assets/images/art/lizardvector_dither.png)

Made this lil wallpaper thing by tracing over the wireframe of a 3D model my friend [Rotlich](https://twitter.com/Satokekkyu) made for me!

