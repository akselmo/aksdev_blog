---
layout: post
title:  "Who is Linux for?"
date:  2021-11-20 17:27:46 +0200
---

A bit of boring rambling. Something I've been thinking about. Not really answering the question directly.
I am talking about **desktop Linux** and not just the server side.
Just remember, these are my **opinions** and nothing factual. I hate that I have to repeat this but hey this is internet,
people like to get mad about anything in here.

Also no, I am not the type who goes around "you must use linuxxxxxx!!!" because I know it's not for everyone. This post should make it pretty clear, but wanted to mention it separately.

## Freedom and relief

For me, the switch from Windows to Kubuntu has been one of the best things I've done when it comes to the
computer life. And honestly, most of my life is computer life. I think about tech all the time, I play games a lot
and so on.

I am, in other words, a nerd. I remember when that word was used as an insult all the time, probably still is somewhere
but not here really. It still has a light negative connotation, as in, "eww a nerd" but nowadays I am just proud if someone
calls me that lol. But yeah, I've been "eww nerd"-ed at many times when I was younger. Kids are evil. :P

I mention this because this is probably what my take is basically: Linux is for us nerds and geeks (still don't kow what
the difference between these words really is, but hey). It's for us, who love learning, who love technology.

For us who love the freedom to switch around things, in both software and hardware.

Freedom is one of those things that I've always believed it comes with a cost. In Linux world, it comes often with the cost of
"agh, this isnt working after all!" which is mostly because the device vendor doesnt support Linux or something else.

## But what do I mean with relief part?

Well, first of all, I am free to update my system when I damn well please. Windows updates were horrific for my anxiety.
Yes, I got anxiety over Windows updates. I am a bit dumb like that. Whenever Windows updated, I was always scared that
I wouldn't get to play games today, or work on something.

Now if that were to happen in Linux, I have freedom to fix the issue myself. Instead of being anxious that
the update breaks something, my mind goes like this: "if it breaks, I can look for help and fix it together with people
on the internet and figure it out, instead of *waiting for some vendor who doesn't really give a shit if I don't have enough money
to fix it*. Reminds me, how many times you have searched help for Windows problems, and there's always some tosser saying "just
run sfc scannow lmaoo bro" and it doesn't do jack shit.

Second relief is privacy and trust oriented. Microsoft, Google and the like deserve zero trust. None of it. They're mindless corporations
and they can collect all the info they want because they have money.

Linux side, I am pretty sure the distro maintainers only care about fixing bugs and not what kind of stuff I searched today to sell me ads.
I mean, it could happen that they track that. But *I trust them not to*. And that is a big relief for me.

In short: **Freedom to repair, which gives me relief.**

## Programming is easier

Programming stuff, especially C, is soooooo much easier to set up and just do things. With Windows you have to install bunch of stuff,
then manually set things in your environment variables, all that jazz.

In Linux, you very likely have compiler already installed in your distro, so you can just start writing code and tinkering with it.

On top of that, most webdev stuff is very easy to install from package managers because the servers use same stuff. Very easy to code,
run and test.

Gamedev side though, at least when it comes to Unity, it can be a bit more rough since you don't have Visual Studio. However,
Visual Studio Code works well on Linux and can be used as an IDE. I've even read people setting up Neovim for Unity programming.
But yes, it takes a bit more effort on that front. Godot runs fine out of the box though!

## But commandline spooky

*First of all, if you're a programmer who is afraid of commandline, I.. I don't know what to say. But anyway.*

Alrrright, let's get this talked over here in the same post, even it's barely on-topic. It's something I always bump into.

You don't need to use terminal all the time, but using it can add a lot of value to your Linux experience.
It is way, WAY easier to help each other when it comes to commandline, since it's pretty much the "only shared standard between distributions."
What this means, is that for most common commands, I can help you with Fedora, even I use Kubuntu, which are different distributions.

Lets say, you want to look at your filesystem in command line, something simple. The same `ls` command works in all common Linux distribution.

Now lets say, you would like to do the same in GUI. Well, what filemanager do you use? Oh, that's way different than mine, I don't know how to use that....

Commandline, and terminal, *unifies* Linux distributions in a level the GUI just can't, since everyone is *free* to change the GUI to anything they prefer (or none at all!).

And this is why I want people to use more Linux commandline. **It helps us to help you!**

~~also makes you look like youre a cool hacker in normies eyes lmao~~

## Remember that Linux will require learning

You have used Windows for most of your life, then suddenly switch to Linux. Of course you're confused, you want to do same stuff like on Windows but.. It's different!

**Of course it is different.** Linux is not Windows. You wouldn't expect Mac to work same as Windows, would you?

It will require learning some new ways to do same things, but luckily those are often simpler. Like installing apps on Linux is literally `sudo apt install appname` in the terminal.
No need to click through billion pages and install some adware accidentally or some shit like that.

Thankfully due to the versatility of Linux distros, you can often set them up very similar to Windows. I personally don't see the appeal, but hey it's there.

## Remember also, the platform is mostly community supported

The Linux desktop side is mostly supported by various communities. By supporting I mean both helping with issues and bugs, but also developing them and fixing them.

There is no giant corporation that pays 23890472903748907829734789€ to fix some bugs (that don't even solve the issue that you were having in the first place...)
No giant corps also to fuck up your system and then ask money to fix it.

You just gotta search the thing that is broken, and ask the people who work on it or use it. Usually the answer is there. If there is no answer and the project is dead,
which unfortunately can happen, you will probably have to change the thing to other one, or fix it on your own.


## You didn't answer the question, who is it for?

* Linux is for the privacy conscious who just want to avoid most telemetry and other crap.

* Linux can be for those who like to play games, thanks to Valve for Proton and Wine. I love games and I can play almost all I like just fine! Native Linux builds run even better than on Windows!

* Linux is for those who just use internet every now and then. A grandma who uses internet just every now and then to pay bills might be just fine with old PC running Linux.

* Linux is for those who want to use old hardware to avoid generating more e-waste.

* Linux is for programmers, who want to do more programming and less setting up your programming environment. I've been way more productive on Linux on this front.

* Linux is for anyone who loves customizing. Because oh boy, you will have time of your life to make your PC look kewl. I know I did! :D

* Linux is for those who don't have the money or just don't want to buy expensive Windows license. You're valid and are allowed to use computers as well.

* **Linux is for us nerds/geeks, tinkerers and hobbyists.** For us, who are not afraid to learn new things.
For us who like helping each other to learn and make computer world a bit better place.

* For us, who feel very strongly about computer things and hate how it's more a corporate minefield nowadays.

In short, if you are interested in learning new things and playing around with new ways of doing stuff, I welcome you warmly! Feel free to ask any questions in the comments :D

*"But I don't want to learn anything, I just want to play games!!"*

Then use Windows. It runs out of the box for that.

## You gatekeeping elitist buttface!!!!!!

Maybe a little, I admit. I'm sorry if I seem that way. I am just tired of people trying Linux, expecting it to be "Windows but good"
but then it turns to be something different and they go on rants how Linux is bad and blabla. It may be bad for your usecase, but it doesn't mean
the OS is bad as a whole.

Just know to temper your expectations and expect to learn a new thing instead of using all your Leet Windows Skillz, and you will be fine.
Some of those skills translate over, but most things, you may have to relearn.

And that's what I am trying to emphasize heavily in this post:

*You must be willing to learn new things to enjoy this.*

I know I am enjoying using Linux way more than Windows. I am learning more of it's use every day and there is nocorporation to stop me from learning because "you are not allowed to modify that, here is an update that breaks everything btw."

**And despite me being all "nerds club" here, I do hope that we get more common users in the Linux ecosystem. They can help us to fix archaic things and make things more streamlined. It is valuable information.**

But learning must be your "drive", I think. If you just want to do your work as always, you're probably better off with what you are already using.

---

*Also there is nothing wrong with using Windows or Mac, to be honest. They both have their uses, their places.
I have to mention this because someone might otherwise get upset that I am dissing their system or whatever. USE WHAT WORKS FOR YOU.*
