---
layout: post
title:  "Some cosplay photos"
date:  2021-09-05 21:01:45 +0300
---

## Disclaimer: I may be cringe but I am free

So I have this lizard partial suit I ordered from here: [https://www.lisanotuspaja.com/en/](https://www.lisanotuspaja.com/en/)

And I love wearing it! I just rarely have time/energy lol.

The character is my own OC Huntra!

Anyways, here's some photos we took today for Tracoff event.

[![Gun](/assets/images/cosplay2021/IMG_4932-3.jpg)](/assets/images/cosplay2021/IMG_4932-3.jpg)

"Where am I?"

[![Chill](/assets/images/cosplay2021/IMG_4934-2.jpg)](/assets/images/cosplay2021/IMG_4934-2.jpg)

"Might as well chill here."

[![Sad](/assets/images/cosplay2021/IMG_4935-2.jpg)](/assets/images/cosplay2021/IMG_4935-2.jpg)

"Peekaboo!"

[![Peekaboo1](/assets/images/cosplay2021/IMG_4953-2.jpg)](/assets/images/cosplay2021/IMG_4953-2.jpg)

[![Peekaboo2](/assets/images/cosplay2021/IMG_4954-2.jpg)](/assets/images/cosplay2021/IMG_4954-2.jpg)

\,,/

[![Metal](/assets/images/cosplay2021/IMG_4966-2.jpg)](/assets/images/cosplay2021/IMG_4966-2.jpg)

[![Metal1](/assets/images/cosplay2021/IMG_4970-2.jpg)](/assets/images/cosplay2021/IMG_4970-2.jpg)

[![Metal2](/assets/images/cosplay2021/IMG_4973-2.jpg)](/assets/images/cosplay2021/IMG_4973-2.jpg)

Thank you [https://twitter.com/tecsiederp/](https://twitter.com/tecsiederp/) for taking and editing the photos!
