---
layout: post
title:  "FOSS communities: You don't need to yell"
date: 2023-02-17 18:18:34 +02:00
---

This is kind of open letter I suppose. I've seen a lot of similar stuff happen in other projects too, like GNOME.
So I wanted to write down what happened to me and my inbox today. It's written in my perspective, but I'm
sure many can sympathize. And I sympathize with them.

# You don't need to yell

KDE Plasma 5.27 landed with my outline change that I made for accessibility reasons mostly.
You can read more about it in here: [https://www.akselmo.dev/2022/10/31/I-made-outlines-for-KDE-Breeze.html](https://www.akselmo.dev/2022/10/31/I-made-outlines-for-KDE-Breeze.html)

They certainly have been dividing opinions. I think that's fine.
I do enjoy good critique and discussion: It's how things get better!

However, what I don't enjoy is:

- Getting my blog comments spammed with "Revert the outlines, they're ugly"
- Asking people to send developers angry messages
- Acting like an complete ass, then hiding behind the whole "but it was critique!" facade.

"Get thicker skin!" you scream. Believe me, my skin is thick enough. I've seen some shit.
But me having a thick skin doesn't mean that you should yell at me for something.

It's demotivating. Why would I want to fix something, if I keep getting screamed at?
What do you expect? It more likely makes us want to *avoid* working on something, since
it's known that fiddling with it will make people angry.

I can understand being angry at faceless corporation that does not listen. KDE is **not** a faceless corporation.
We're a group that does this because we enjoy what we do and want to make cool things usable for everyone.
And we listen.

Trust me, we're discussing this and gathering feedback on the outline feature. And every other feature as well.

All I ask in return is to behave and be patient. The iterations will come, if needed. Changes, fixes, and all the cool stuff.
I'm not here to be yelled at. Well written critique is allowed and appreciated, but be respectful:
No need to kiss my feet, but no need to scream at me either. Makes it easier for everyone involved.

When thinking of future, I am planning to make the separators colorable in color schemes. This means that the outlines and other separators
can use that color and user can dictate themselves the color completely. It would also help with separator coloring
in Plasma themes, not just Window Decorations.

Here's a merge request I'm working on addressing the outline intensity: [https://invent.kde.org/plasma/breeze/-/merge_requests/292](https://invent.kde.org/plasma/breeze/-/merge_requests/292)

~~However for now, I don't have time to work on KDE stuff. I'm working on my game, I'm joining a gamejam this weekend, I'm busy with life and work.~~

But if you take anything out of this post: **My blog is not an issue tracker for KDE. LOL.**
Use [https://bugs.kde.org/](https://bugs.kde.org/) instead.

We can do better than this. We *must* do better than this.

---

PS. If you want to modify the color of the outline, you can set the titlebar color to something different, and the outline color
will follow it.

PPS. I may remove the comments from my blog at some point and ask people use the public mailbox linked below instead. Maybe. Idk.