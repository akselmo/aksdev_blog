---
layout: post
title:  "Why I find Linux gaming important?"
date:  2022-02-05 00:03:25 +0200
---

People who read this mess of a blog may have noticed that I am very Linux-y. This wasn't actually always the case.

I used to be very comfort oriented. Didn't mean that I didn't like to tinker with things, but more like
when I got home I just wanted to play games and that's it. Click and play.

I tried Linux desktop back then and I enjoyed using it but what held me back was gaming. I needed my
games, especially as a youngster who just wanted to play cool_new_thing with friends. Sure there were
ways to get games to work, like PlayOnLinux, but it was too much for me.

Nowadays though, my thoughts have shifted. I rather tinker with my device a little to play
some game, just so I can avoid Microsoft or other BigCorp. Maybe my rebellious phase is kicking
in way later, I don't know. I never had that as a teen lol.

But I think Linux gaming is way more than that, and here's few points why I find Linux gaming
very important for the future of games.

## Competition in PC platform space

Big part of computer world, as evident by the massive Steam user count, is gaming. I am young,
but not too young to remember DOS era. Even back then, games were a thing. And those games were great, by the way.

The unfortunate thing in PC platform is that there's not much competition. There's two big names,
Mac and Windows. And if you play games, it's almost always Windows.

Now that Linux gaming is rising, we can FINALLY start thinking about alternatives. We can finally
tell MS that hey, maybe we don't like all this telemetry you're doing. I just want to play games,
I don't want my PC to spend it's resources doing whatever your telemetry stuff is doing. I want all
that perf for my games. And that's what Linux distros let me do.

Now hopefully, Linux desktop as a gaming platform keeps rising. Even more hopefully PC manufacturers
will give option to install some distro instead of Windows when you buy a PC, when they
realise that gamersTM really are into it out of sudden.

That's the dream anyway. I know reality is harsh, but maybe Steam Deck will help people
learn that there's more to computers than Windows.

And maybe in return MS steps up and makes better Windows. I'm not switching anytime soon, hopefully
never, but I hope this will make the platform better for my friends who use it.

## Games taught me a lot

Games have taught me a lot about computers, programming and of course English language. Even some
social stuff!

How does this tie into Linux desktop? The barrier of entry to computers nowadays is
hardware + OS, especially if you want to game. Shaving that extra 100€ (Windows price) from the computer
sale will help many who are getting their new PC, especially younger people.

What I'm trying to say that when the OS is free, the barrier of entry lowers down,
and this helps younger people to learn more since they can spend that extra 100€ for a game or two.

In general it lowers the barrier of entry, not just gaming but everyone everywhere. The only main problem
I see with this is that the main Linux distros need to keep up their newbie friendliness and make it better
and easier for new users whenever they can.

## Windows Live Gold can happen again any time

Remember that thing? Remember Games for Windows Live?

Remember when MS wanted people to pay some dumb subscription to play PC games online
like you have to do on XBox?

Nothing is stopping MS from doing that again. There may be huge backlash, but it doesn't stop
them.

If more people switch to Linux for gaming, especially IF that happens, it sends a clear signal
to MS.

Having a whole gaming system in hands of one corporation is **bad.**

## Linux can wake up older PC's for gaming

Win10 was pretty rough on my +5 year old PC I gave for my sister for gaming. It was super slow
and clunky as heck, considering it has still old HDD, instead of SSD.

Had I installed some Linux distro for it, I think she would have better time using that PC.
If I did that again now I would slap Ubuntu on it with Minecraft pre-installed.

This also lowers e-waste when people don't have to buy he newest thing just to run the OS.
TPM 2.0 anyone?? :P

## More users for Linux userspace

The more the merrier, I think. Every user is a possible contributor.

Heck, without Linux gaming being a thing, I wouldn't have changed my OS, I would
never report some of the bugs I've seen, I would never contribute to the OS in a way or another...

I am even thinking of making my next game Linux first title. (Well I've basically decided it already, since I develop it on Linux..)

Had Linux gaming not been a thing like it is now, I would've just continued making games on Windows and never
contributed those bug reports or stuff like [ESO Linux Addon Manager](https://github.com/Akselmo/ESOLinuxAddonManager).

I am not trying to say that "you should be glad i switched!!11" Just think the people we can get around here to contribute
to the software we use daily. And some of them like to play games on their free time.

## Everyone deserves privacy

Linux and FOSS in general are very privacy-first oriented software.
On other hand, proprietary OS like Mac and Windows are not (despite what they want you to think).

Everyone deserves to have privacy if they want it, even the kids who just wanna dab on noobs in Fortnite.

Privacy is a human right, after all.

# But wait, what about proprietary gaming????

Someone asked me once why do you give proprietary games a free pass? Why should they be allowed in our free software?

I believe strongly that basic software and needs in computer world, like using internet, office software, chatting, learning and studying etc. etc.
should be free for everyone. Both free as in freedom and beer.
Everyone should have an access to same tools to build their careers, talk to their relatives and friends and so on.

But games are art and unfortunately, for now, art is a luxury.
Luxury is something that we don't *need* to live.
I would love to see every software ever made open source of course, but it will take time to reach that goal.

We have to start from somewhere, and I believe getting the basic necessities in order at first is a huge
step alone. We need to concentrate on that at first.

Of course this doesn't mean to stop making FOSS games, I am making my own as well. FOSS engines and games
deserve all the love and care they get. Please keep making them.

But my priorities are: necessities first, then we can focus on luxuries. Hopefully one day
we can share luxuries around without worry.

That's why I give proprietary games a free pass:
*It's not my priority to get games FOSS. It's all the basic stuff first.*

# I want Linux gaming to succeed, what do?

* Buy games from developers who make Linux games!
* Donate to FOSS game engines like Godot!
* Also **USE** FOSS game engines! Tell people about them.
* Switch to Linux in your older laptop for example, try it out. Play some games with it
* Support open platforms like Steam Deck or other Linux based gaming platforms.
* Ask developers for Linux support. Yes I know this may go to deaf ears, but asking is free.
    * Indie devs are more lenient in this, usually!
* Try open source games and software even on Windows!
* If you feel like you're ready to switch, give dual booting a try!
    * I have a guide for Kubuntu here: [Setting up Kubuntu for gaming](https://www.akselmo.dev/2021/11/12/Setting-up-Kubuntu-for-gaming.html)


*Thanks for reading! I've wanted to make this post for a long while but didn't have the energy until now.*
