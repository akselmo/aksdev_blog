---
layout: post
title:  "My character and Matrix server"
date:  2021-12-02 19:46:06 +0200
---

Just a quick post, I wanted to share two things.

# Commissioned art

First, I got this really nice commission of my character Huntra wearing Linux shirt.
I really like [Xenia](https://xenia-linux-site.glitch.me/) so I wanted to see my
character in something similar.

[![HuntraLinux](/assets/images/oc/HuntraLinux.png)](/assets/images/oc/HuntraLinux.png)

Thank you for making this for me [Saikkunen](https://saikkuart.tumblr.com/)!
Go commission them!!! :D

# Matrix channel

I started running a Matrix channel for anyone who may read this blog or follow me on
Twitter/Mastodon and is interested in techy FOSS-y things and my games! It's bridged to
my Aks_Dev discord!

You're welcome to join, just behave!

[Aks_Dev -matrix channel](https://matrix.to/#/#aksdev:matrix.org)

*Yes it was called Cool Tech Nerds at some point but instead running two separate Matrix channels I just wanted to run one.*
