---
layout: post
title:  "I am tired of social media"
date:  2021-09-24 21:45:55 +0300
---

Yeah, not uncommon these days.

I am so tired of using Twitter and being flooded with random, uninteresting crap. The algorithm still does it's thing even when you have "latest tweets" enabled.

On top of that, the muted words thing works like ass. It sometimes filters out words, sometimes doesn't.

And the ads don't seem to respect those filters, of course.

So I gave up with Twitter. I still have mine up, but I'm on [Mastodon](https://mastodon.technology/@huntra) now and I use it as my main thing. There are actually interesting topics discussed there and no algos telling me to buy newest spyware software or w/e.

Mastodon just doesn't try to keep you constantly "enganged" (addicted) on their platform. It doesn't try to keep you "doomscrolling" so you see more ads. It doesn't have ads. It just is a place where people can toot their thoughts, announce things, that's about it. And I'm super happy with that. *And I can also use it from my terminal!!!* It's kinda fun to just type `toot post` in my terminal and share my brainfarts. I tried some Twitter related terminal apps but none of them really worked.

I also use [Moa Bridge](https://moa.party/) so I can just send my toots/tweets in both platforms with specific hashtag. I just use *#xp* as my hashtag for that.

Another place I've stopped using as much is Youtube. Youtube used to be a video site with some comments and such. Now it's more a social media place, also trying to get you addicted to their content to watch more ads and make more bank. I stopped using Youtube from browser, but instead use [Freetube](https://freetubeapp.io/). No ads, no BS, just list of videos and it plays them through Invidious instances so there's (probably) not any tracking either. Also it has sponsor skip integrated to it, which is nice. On mobile I use [NewPipe](https://newpipe.net/) which is basically the same, but on mobile.

On IM side, I've started using [Matrix.org](https://matrix.org/) with my fiancee. It has end-to-end encryption, which helps with my trust issues, but I'm more into the freedom of choosing your client. I can use Matrix from my [Weechat](https://weechat.org/) that has my IRC channels too, or I can use [nheko](https://github.com/Nheko-Reborn/nheko) which is more feature rich. On my phone I use [Fluffychat](https://fluffychat.im/en/).

I have also added many [Discord-Matrix](https://matrix.org/docs/projects/bridge/matrix-appservice-discord) bridges to my servers, so I can see what my Discord friends are doing even in Matrix. That however doesn't support E2E-encryption.

Is this all a big hassle to set up? Not really, in my opinion at least, but it may require some tinkering. But it gives me freedom of choice and I value that more than just simply starting something. Besides, I like tinkering!

Anyways, **these alternatives let me choose when I want to use these services**, instead of the services telling me "HEY LOOK NEW NOTIFICATION PLEASE CLICK ME NOW AND WATCH FEW ADS AT THE SAME TIME GOD PLEASE." Sure, you could go "just ignore it" and I did, but there's still the tracking issues n all that shit. And ignoring the problem doesn't solve the problem, just moves it to later date.

I am more comfortable these open source solutions and that I can choose things.

In short I'll be taking (hopefully indefinite) break from Twitter, but **will crosspost important game/other announcements** there. I don't like it anymore. Too noisy. See you in Mastodon!

ps. My [open source FPS](https://github.com/Akselmo/artificial-rage) project has been really fun to do and it's going forward slowly but surely. My secret game project also is on route, but I let artists finish their work before I really sit down and work on it. But it's all there and will happen, some day. :)
