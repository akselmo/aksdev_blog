---
layout: page
title: About Me
permalink: /about/
---
<img src="/assets/images/oc/HuntraLinux.png" alt="My character Huntra made by Saikkunen" width="40%" align="right"/>

Welcome to my blog! Here I ramble about gamedev and programming.

I am a Finnish guy in my late 20s and I work as a design engineer in my day job.
But on my free time I make games and play them!

When it comes to politics, I try to avoid it all as well as I can, except on FOSS and privacy matters I will probably be a bit louder.

In short: *Do good, be good to others, share good, fun and useful things. That's all I try to be.*

[![mastodon](https://img.shields.io/mastodon/follow/109598121971865878?color=%233088D4&domain=https://tilde.zone&logo=mastodon&style=flat-square&logoColor=white)](https://tilde.zone/@aks)

[![itchio](https://img.shields.io/badge/itch.io-akselmo-%23FA5C5C?style=flat-square&logo=itch.io&logoColor=white)](https://akselmo.itch.io/)

[![ko-fi](https://img.shields.io/badge/ko--fi-donate-%23FF5E5B?style=flat-square&logo=ko-fi&logoColor=white)](https://ko-fi.com/L4L57FOPF)

[![Liberapay patrons](https://img.shields.io/liberapay/patrons/akselmo?label=LiberaPay&logo=liberapay&logoColor=ffffff&style=flat-square)](https://liberapay.com/akselmo/)

[![matrix](https://img.shields.io/matrix/aksdev-space:matrix.akselmo.dev?color=0dbd8b&label=aks_dev%20matrix&logo=matrix&logoColor=ffffff&server_fqdn=matrix.org&style=flat-square)](https://matrix.to/#/#aksdev-space:matrix.akselmo.dev)

Icon was made by [https://twitter.com/WeirdAniki/](https://twitter.com/WeirdAniki/status/1594061358122864640)! Thank you!

Feel free to use the banner below to link to my site!

 <a href="https://www.akselmo.dev/"><img class="crispy" src="/assets/images/stamp/stamp.png" alt="akselmo"></a>

 Also, if you want to email me, whatever it may be (just be nice or i'll cry), here's my address <a href="mailto:akselmo@akselmo.dev">akselmo@akselmo.dev</a>.

## Friends

Here's links to my friends sites!

<a href="https://b266.ca/"><img class="crispy" src="http://transistorcafe.net/~vivi266/tc-banner88.png" alt="b266"></a>
<a href="https://dee-liteyears.neocities.org/"><img class="crispy" src="https://dee-liteyears.neocities.org/deebutton1.png" alt="dee-liteyears"></a>
<a href="https://samuele963.github.io/"><img class="crispy" src="https://dee-liteyears.neocities.org/sam963.png" alt="samuele963"></a>
<a href="https://nokia64.neocities.org/"><img class="crispy" src="https://nokia64.neocities.org/img/buttons/nokia64.png" alt="nokia64"></a>
<a href="https://sirlan-tomma.neocities.org/"><img class="crispy" src="https://sirlan-tomma.neocities.org/buttons/sirlan.gif" alt="sirlan"></a>

## Webrings

I may not agree with all the opinions expressed on the webrings, but there are many cool sites to explore through.
The creativity people have is amazing!

# Yesterweb

<script src="https://yesterwebring.neocities.org/script-text.js"></script>
<webring-css site="https://www.akselmo.dev/"></webring-css>
<style>
:root {
--link-weight:strong;
--mylinkcolor:#28daf1;
--linksize:11px;
--link-decoration:none;
--link-family:"Hack";
--text-color:#b0b0b0;
}
</style>

# Geekring

<a href="http://geekring.net/site/NUMBER/previous">Previous site</a> --
<a href="http://geekring.net/site/NUMBER/random">Random site</a> --
<a href="http://geekring.net/site/NUMBER/next">Next site</a>

